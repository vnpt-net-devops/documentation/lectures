kubectl apply -f efk/

kubectl port-forward [kibana-pod-name] 5601:5601 --namespace=kube-logging

# visit the following web URL:

http://localhost:5601

# testing
kubectl create -f counter.yaml